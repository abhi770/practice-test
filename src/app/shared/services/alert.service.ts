// import { Injectable } from '@angular/core';
// import { SweetAlertIcon } from 'sweetalert2';
// import Swal from 'sweetalert2';

// @Injectable({
//   providedIn: 'root',
// })
// export class AlertService {
//   constructor() {}
//   show_alert(
//     alert_title = '',
//     alert_text = '',
//     alert_icon: SweetAlertIcon = 'success'
//   ) {
//     Swal.fire(alert_title, alert_text, alert_icon);
//   }
//   warn(swalParam: Partial<SwalParam>){
//     swalParam.icon = 'warning'
//     swalParam.title = swalParam.title ?? 'Are you Sure'
//     return this.dialog(swalParam)
//   }

//   wentWrong(error: any = {}){
//     this.error({message: 'Problem with service please try later'})
//     console.log(error);
//   }
//   error(swalParam: Partial<SwalParam>){
//     swalParam.icon = 'error'
//     swalParam.title = swalParam.title || 'Error'
//     return this.dialog(swalParam)
//   }
//   success(swalParam: Partial<SwalParam>){
//     swalParam.icon = 'success'
//     swalParam.title = swalParam.title || 'Success'
//     return this.dialog(swalParam)
//   }

//   dialog(swalParam: Partial<SwalParam>){
//     return Swal.fire(swalParam.title, swalParam.message, swalParam.icon);
//   }
//   showValidationErrors(errors: any) {
//     let prepareError = '<h4 class="alert-heading">There are some errors</h4>';
//     if (typeof errors == 'object') {
//       for (const property in errors) {
//         prepareError +=
//           '<div class="text-danger">' + errors[property] + '</div>';
//       }
//     } else {
//       prepareError += '<div class="text-danger">' + errors + '</div>';
//     }
//     Swal.fire({ html: prepareError });
//   }
// }
// export interface SwalParam {
//   title:string,
//   message:any,
//   icon: 'success' | 'warning' | 'error'
// }
