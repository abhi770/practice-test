import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class RoutingService {
  constructor(private router: Router, private location: Location) {}

  goBack() {
    this.location.back();
  }

  dynamicPath(path: string, params: string = '') {
    if (params) {
      this.route(path, false, { id: params });
    } else {
      this.route(path);
    }
  }

  route(path: string, replaceUrl: boolean = false, data?: any) {
    if (!data) {
      this.router.navigate([path], { replaceUrl });
    } else {
      this.router.navigate([path], {
        replaceUrl,
        queryParams: data,
      });
    }
  }
}
