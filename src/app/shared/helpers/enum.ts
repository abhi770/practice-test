export enum Path {}
export enum Toaster {
  success = 'success',
  warning = 'warning',
  danger = 'danger',
}