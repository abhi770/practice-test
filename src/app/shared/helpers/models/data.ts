import { JobDTO } from './jobs';

export class DataDTO {
  warning: string = '';
  legalNotice: string = '';
  jobCount: string = '';
  jobs: JobDTO = new JobDTO();
}
