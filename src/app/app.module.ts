import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Broadcaster } from './shared/helpers/providers/broadcaster';
import { StorageService } from './shared/services/storage.service';
import { UtilService } from './shared/services/util.service';
import { RoutingService } from './shared/services/routing.service';
import { HttpService } from './shared/services/http.service';
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxPaginationModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
  ],
  providers: [
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: AuthInterceptor,
    //   multi: true,
    // },
    HttpClient,
    HttpService,
    RoutingService,
    StorageService,
    UtilService,
    Broadcaster,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
