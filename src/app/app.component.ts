import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { JobDTO } from './shared/helpers/models/jobs';
import { UtilService } from './shared/services/util.service';
import { RoutingService } from './shared/services/routing.service';
import { HttpService } from './shared/services/http.service';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  jobsData: JobDTO[] = [];
  getJson: any;
  totalCount: number = 0;
  count: number = 10;
  page: number = 1;
  categoriesArray: any[] = [];
  companiesArray: any[] = [];
  jobTypesArray: any[] = [];
  jobTypeFilteredSelected: string = 'undefined';
  companiesFilteredSelected: string[] = [];
  categoriesFilteredSelected: string = 'undefined';
  dateFilteredSelected: string = '';
  dropdownCompany: boolean = false;
  searchInput: string = '';
  private searchSubject = new Subject<string>();

  constructor(
    public util: UtilService,
    public routing: RoutingService,
    private http: HttpService
  ) {
    this.searchSubject
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        filter((searchText) => !!searchText)
      )
      .subscribe((searchText: any) => {
        this.filterJobs(searchText);
      });
  }

  async ngOnInit() {
    this.getJson = this.util.getJson();
    this.jobsData = this.getJson.jobs;
    this.valueReseting();
    await this.getFilterJobTypes();
    await this.getFilterCompanies();
    await this.getFilterCategories();
  }

  DropdownToggleCompany() {
    this.dropdownCompany = !this.dropdownCompany;
  }

  async getFilterJobTypes() {
    var uniqueJobType = new Set();
    this.getJson.jobs.forEach(function (job: any) {
      if (job.job_type !== '') {
        uniqueJobType.add(job.job_type);
      }
    });
    this.jobTypesArray = Array.from(uniqueJobType);
  }

  async getFilterCompanies() {
    var uniqueCompanies = new Set();
    this.getJson.jobs.forEach(function (job: any) {
      if (job.company_name !== '') {
        uniqueCompanies.add(job.company_name);
      }
    });
    this.companiesArray = Array.from(uniqueCompanies);
  }

  async getFilterCategories() {
    var uniqueCategories = new Set();
    this.getJson.jobs.forEach(function (job: any) {
      if (job.category !== '') {
        uniqueCategories.add(job.category);
      }
    });
    this.categoriesArray = Array.from(uniqueCategories);
  }

  async pageChanged(p: any) {
    this.page = p;
  }

  getFilteredSearched() {
    this.getAllfilterWorking();
  }

  getFilteredCompanySearched(data: any) {
    this.companiesFilteredSelected.push(data);
    this.getAllfilterWorking();
    this.dropdownCompany = false;
  }

  async onSearchInputChanged(event: Event) {
    if ((event.target as HTMLInputElement).value) {
      const searchText: any = (event.target as HTMLInputElement).value;
      this.searchSubject.next(searchText);
    } else {
      this.resetComapanies();
    }
  }

  filterJobs(searchText: string) {
    this.searchInput = searchText;
    let data: any = this.companiesArray;
    this.companiesArray = data.filter((job: any) =>
      job.toLowerCase().includes(searchText.toLowerCase())
    );
  }

  getAllfilterWorking() {
    this.jobsData = [];
    if (this.dateFilteredSelected) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return job.publication_date.split('T')[0] === this.dateFilteredSelected;
      });
    }
    if (this.jobTypeFilteredSelected !== 'undefined') {
      this.jobsData = this.getJson.jobs.filter((x: any) => {
        return x.job_type == this.jobTypeFilteredSelected;
      });
    }
    if (this.companiesFilteredSelected.length) {
      this.jobsData = this.getJson.jobs.filter((x: any) => {
        return this.companiesFilteredSelected.includes(x.company_name);
      });
    }
    if (this.categoriesFilteredSelected !== 'undefined') {
      this.jobsData = this.getJson.jobs.filter((x: any) => {
        return x.category == this.categoriesFilteredSelected;
      });
    }
    if (
      this.categoriesFilteredSelected !== 'undefined' &&
      this.companiesFilteredSelected.length
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          job.category === this.categoriesFilteredSelected &&
          this.companiesFilteredSelected.includes(job.company_name)
        );
      });
    }
    if (
      this.companiesFilteredSelected.length &&
      this.jobTypeFilteredSelected !== 'undefined'
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          job.category === this.categoriesFilteredSelected &&
          job.job_type === this.jobTypeFilteredSelected
        );
      });
    }
    if (
      this.companiesFilteredSelected.length &&
      this.jobTypeFilteredSelected !== 'undefined'
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          this.companiesFilteredSelected.includes(job.company_name) &&
          job.job_type === this.jobTypeFilteredSelected
        );
      });
    }
    if (
      this.dateFilteredSelected &&
      this.jobTypeFilteredSelected !== 'undefined'
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          job.job_type == this.jobTypeFilteredSelected &&
          job.publication_date.split('T')[0] === this.dateFilteredSelected
        );
      });
    }
    if (this.dateFilteredSelected && this.companiesFilteredSelected.length) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          this.companiesFilteredSelected.includes(job.company_name) &&
          job.publication_date.split('T')[0] === this.dateFilteredSelected
        );
      });
    }
    if (
      this.dateFilteredSelected &&
      this.categoriesFilteredSelected !== 'undefined'
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          job.category === this.categoriesFilteredSelected &&
          job.publication_date.split('T')[0] === this.dateFilteredSelected
        );
      });
    }
    if (
      this.dateFilteredSelected &&
      this.jobTypeFilteredSelected !== 'undefined' &&
      this.companiesFilteredSelected.length
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          this.companiesFilteredSelected.includes(job.company_name) &&
          job.job_type === this.jobTypeFilteredSelected &&
          job.publication_date.split('T')[0] === this.dateFilteredSelected
        );
      });
    }
    if (
      this.dateFilteredSelected &&
      this.jobTypeFilteredSelected !== 'undefined' &&
      this.categoriesFilteredSelected !== 'undefined'
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          job.category === this.categoriesFilteredSelected &&
          job.job_type === this.jobTypeFilteredSelected &&
          job.publication_date.split('T')[0] === this.dateFilteredSelected
        );
      });
    }
    if (
      this.dateFilteredSelected &&
      this.companiesFilteredSelected.length &&
      this.categoriesFilteredSelected !== 'undefined'
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          job.category === this.categoriesFilteredSelected &&
          this.companiesFilteredSelected.includes(job.company_name) &&
          job.publication_date.split('T')[0] === this.dateFilteredSelected
        );
      });
    }
    if (
      this.jobTypeFilteredSelected !== 'undefined' &&
      this.companiesFilteredSelected.length &&
      this.categoriesFilteredSelected !== 'undefined'
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          job.category === this.categoriesFilteredSelected &&
          this.companiesFilteredSelected.includes(job.company_name) &&
          job.job_type === this.jobTypeFilteredSelected
        );
      });
    }
    if (
      this.categoriesFilteredSelected !== 'undefined' &&
      this.companiesFilteredSelected.length &&
      this.jobTypeFilteredSelected !== 'undefined' &&
      this.dateFilteredSelected
    ) {
      this.jobsData = this.getJson.jobs.filter((job: any) => {
        return (
          job.category === this.categoriesFilteredSelected &&
          this.companiesFilteredSelected.includes(job.company_name) &&
          job.job_type === this.jobTypeFilteredSelected &&
          job.publication_date.split('T')[0] === this.dateFilteredSelected
        );
      });
    }
    this.valueReseting();
  }

  checkCompany(item: any) {
    return this.companiesFilteredSelected.find((x) => item === x);
  }

  removeCompany(item: any) {
    let index = this.companiesFilteredSelected.findIndex((x) => item === x);
    if (index !== -1) {
      this.companiesFilteredSelected.splice(index, 1);
      console.log(this.companiesFilteredSelected);
    }
    if(!this.companiesFilteredSelected.length){
      this.resetComapanies();
    }
  }

  reset() {
    this.jobsData = [];
    this.jobsData = this.getJson.jobs;
    this.jobTypeFilteredSelected = 'undefined';
    this.companiesFilteredSelected = [];
    this.categoriesFilteredSelected = 'undefined';
    this.dateFilteredSelected = '';
    this.valueReseting();
  }

  resetComapanies() {
    this.searchInput = '';
    this.companiesFilteredSelected = [];
    this.getFilterCompanies();
    this.dropdownCompany = false;
    this.reset();
  }

  valueReseting() {
    this.totalCount = this.jobsData.length;
    this.page = 1;
  }
}
